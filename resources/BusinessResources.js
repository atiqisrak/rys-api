// const BusinessResources = async (data) => {
//     return data.map((element) => {
//       return {
//         id: element.id,
//         customer_name: element.name,
//         customer_email: element.email,
//         dob: element.birthDate,
//       };
//     });
//   };

  class BusinessResources {
      async whenLoad(data){

        // if(data.length != 0){
          return data;
        //     if(selectItem.length > 0){
        //       const newarr = []
        //       selectItem.map(dt => {
        //         newarr.push({[data[dt]]: data[dt]})
        //       })
        //       return newarr;
        //     }
        // }
      }
      async handleCollection(collection) {
        // if(collection.length == 1) handleSingleObject([...collection])
        return collection.map((element) => {
          return {
            id: element.id,
            business_name: element.businessName,
            short_description: element.shortDescription,
            long_description: element.longDescription,
            business_type: element.businessType,
            service_type: element.serviceType,
            country: element.country,
            city: element.city,
            gl_location_link: element.locationPoint,
            business_category: element.businessCategory,
            business_owner: element.businessOwner,
            business_manager: element.businessManager,
            number_of_employee: element.numberOfEmployee,
            trade_licence: element.tradeLicence,
            tin: element.tin,
            bin: element.bin,
            user: element.user,
            status: element.status == true ? 'Active' : 'Deactive',
            created_at: element.createdAt,
            updated_at: element.updatedAt
          };
        });
      }
    
      async handleSingleObject(obj) {
          return {
            id: obj.id,
            business_name: obj.businessName,
            short_description: obj.shortDescription,
            long_description: obj.longDescription,
            business_type: obj.businessType,
            service_type: obj.serviceType,
            country: obj.country,
            city: obj.city,
            gl_location_link: obj.locationPoint,
            business_category: obj.businessCategory,
            business_owner: obj.businessOwner,
            business_manager: obj.businessManager,
            number_of_employee: obj.numberOfEmployee,
            trade_licence: obj.tradeLicence,
            tin: obj.tin,
            bin: obj.bin,
            status: obj.status == true ? 'Active' : 'Deactive',
            created_at: obj.createdAt,
            updated_at: obj.updatedAt
        };
    }
}
export default BusinessResources;