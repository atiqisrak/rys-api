const { slugify } = require("../lib/helper");

class AssetService {
    async readyData(data) {
        const fields = ['business_id','asset_type','property_name','country','city', 'location_point','geo_tag','no_of_room'] 
        const result = {};
        for (const key of fields) {
            result[key] = data[key];
            if(result[key] == data[key]){
                result['business'] = {
                    connect:{
                        id:data.business_id
                    }
                }
            }
        }
        result['slug'] = slugify(data['property_name']);
        delete result['business_id']
        return result;
    }
}
export default AssetService;