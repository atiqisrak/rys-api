const bcrypt = require('bcryptjs');
const axios = require('axios');
import {generateUserToken} from "./JWTservice"
// const jwt = require('jsonwebtoken');
import prisma from "../lib/prisma";

export const getUser = async(requestData) => {
    const user = await prisma.user.findFirst({
        where: {
          phoneNumber: requestData.phoneNumber
        }
      })
      return user;
    }
export const getUserByEmail = async(requestData) => {
    const user = await prisma.user.findFirst({
        where: {
          email: requestData.email
        }
      })
      return user;
    }

    
  export const findOrCreateUser = async (requestData) => {
  let user = await prisma.user.findFirst({
    where: { 
      phoneNumber: requestData.phoneNumber
    },
  });

  if (!user) {
    user = await prisma.user.create({
      data: {
        email:"user@gmail.com",
        phoneNumber: requestData.phoneNumber
      },
    });
  }
  return user;
};

    export const getUserByOtp = async() => {
      const user = await prisma.user.findFirst({
        where: {
          phoneNumber: requestData.phoneNumber
        }
      });
      if (!user) return json({ error: 'Incorrect login',status: 400 });
      return user;
    }

    export const getUserByCred = async() => {
      const user = await prisma.user.findFirst({
        where: {
            email: email
        }
      });
      if (!user || !(await bcrypt.compare(password, user.password))) return json({ error: 'Incorrect login',status: 400 });
      return user;
    }

    export const sendOtp = async(userData) => {
      // Get the current time
      const currentTime = new Date();
      // Set the expiration time to 3 minutes from now
      const expirationTime = new Date(currentTime.getTime() + 30 * 60 * 1000);
      const code = Math.floor(1000 + Math.random() * 9000)
      // return res.status(200).json({ message: code+' OTP send to your number' });
      const user = await prisma.user.update({
        where: {
            id: userData.id
        },
        data: {
          otp : code,
          otpExpireAt : expirationTime,
          loggerType: "APPS_USER"
        }
      });
      // write sms api uhl churaya
      let params = {
        "senderId": "8809638112244",
        "is_Unicode": true,
        "is_Flash": false,
        "message": "Ryserve: Your OTP is "+code,
        "mobileNumbers": "88"+user.phoneNumber,
        "apiKey": "FdMUiVSDor5vYnFIUcxUAHF0QeoTkE3b9+4PA3QgBx8=",
        "clientId": "5bdbb24f-263c-4ca3-953c-3448b967089e"
      };
      const result = axios.post('https://sms.novocom-bd.com/api/v2/SendSMS',params)
      // .then(function (response) {
      //   console.log(response);
      // })
      // .catch(function (error) {
      //   console.log(error);
      // });
      // const result = await fetch('https://sms.novocom-bd.com/api/v2/SendSMS', {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/json'
      //   },
      //   body: JSON.stringify(params),
      // })
      // const data = await result.json()
      return user;
    }
    export const setSessionData = async(userData) => {
      // return userData;
      var date = new Date(); // Now
       date.setDate(date.getDate() + 365)
      const isoDateString = date.toISOString();
      // const {id} = userData
      const existSession = await prisma.session.findFirst({
          where: {
            userId: userData.user.id
          }
        });
        // return existSession;
        let session;
        if(existSession){
          session = await prisma.session.update({
            where: {
              id: existSession.id,
            },
            data: {
                sessionToken: userData.token
            }
          });
          // return session;
        } else {
          session = await prisma.session.create({
            data: {
                // userId: userData.id,
                user: { connect: { id: userData.user.id } },
                sessionToken: userData.token,
                expires: isoDateString,
            }
          });
        }
        return session;
    }
    export const createUserSession = async(userData) => {
      const token = await generateUserToken(userData)
        return token;

        // const session = await storage.getSession()
        // session.set('user', user)
        // return redirect(redirectTo, {
        //   headers: {
        //     'Set-Cookie': await storage.commitSession(session),
        //   },
        // })
    }

    export const userUpdate = async(userData,userId) => {
      // return userData;
      const user = await prisma.user.update({
        where: {
          id: userId
        },
        data: {
          name : userData.name,
          email : userData.email,
          birthDate : new Date(userData.birthDate),
          status : true,
          // otp : null,
          otpExpireAt : null
        }
      });
      const dt = await generateUserToken({...user,...{platform:userData.platform}})
      const tokenUser = {user, ...{token:dt}};
      await setSessionData(tokenUser)
      return tokenUser;
    }

    export const validateEmail = async(email) => {
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email.length || !validRegex.test(email)) {
          return "Please enter a valid email address"
        }
    }