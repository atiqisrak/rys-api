import { slugify } from "../lib/helper";
import prisma from "../lib/prisma";

export const addAmenities = async(amenitiesData) => {
    try {
        const amenities = await prisma.amenities.createMany({
            data: amenitiesData.data.map((amenity) => ({
                businessId: amenitiesData.businessId,
                ...amenity,
              }))
        });
        return amenities; 
    } catch (error) {
        console.log('Error creating Amenities:', error);
    }
}

export const getAllAmenities = async() => {
    const amenities = await prisma.amenities.findMany({
        include: {
            business: {
              select: {
                id: true,
                businessName: true,
                businessType: true
              },
            },
        },
    })
    // return business;
    return amenities;
}
export const getSingleAmenities = async(id) => {
    const amenities = await prisma.amenities.findMany({
        where: {
            id:id
        },
        include: {
            business: {
              select: {
                id: true,
                businessName: true,
                businessType: true
              },
            },
          }
    })
    return amenities;
}

export const updateAmenities = async(id,data) => {
    try {
        const amenities = await prisma.amenities.update({
            where:{
                id: id,
            },
            data:{
                acRoom: data.ac_room,
                reception: data.reception,
                swimmingPool: data.swimming_pool,
                laundryService: data.laundry_service,
                airportShuttle: data.airport_shuttle,
                gym: data.gym,
                parking: data.parking,
                kitchen: data.kitchen,
                smoking: data.smoking,
                pets: data.pets,
                cctv: data.cctv,
                wifi: data.wifi,
                business:{
                    connect:{
                        id: data.businessId
                    }
                },
                status: data.status
            }
        });
        return amenities;
    } catch (error) {
        console.log('Error updating post:', error);
    }
}
export const destroyAmenities = async(id) => {
    const amenities = await prisma.amenities.delete({
        where:{
            id: id
        }
    });
    return amenities;
}