import prisma from "../../lib/prisma";
export const getUser = async(userEmail) => {
    const user = await prisma.user.findFirst({
        where: {
            email: userEmail,
        }
    });
    return user;
}