import { slugify } from "../../lib/helper";
import prisma from "../../lib/prisma";

export const addAsset = async(data) => {
    try {
        const asset = await prisma.asset.create({
            data: {
                assetType: data.asset_type,
                propertyName: data.property_name,
                slug: slugify(data.property_name),
                business: {
                    connect: {
                        id: data.businessId
                    }
                },
                country: data.country,
                city: data.city,
                locationPoint: data.location_point,
                geoTag: data.geo_tag,
                noOfRoom: data.no_of_room,
                about: data.about,
                status: data.status
            }
        })
        return asset;
    } catch (error) {
        return error;
    }
}
export const getAllAsset = async(userId) => {
    const assets = await prisma.asset.findMany({
        where:{
            business: {
                userId: userId 
            }
        },
        include:{
            business: {
                select: {id:true,businessName:true}
            }
        }
    })
    return assets;
}

export const getSingleAsset = async(id) => {
    const assets = await prisma.asset.findMany({
        where:{
            id:id
        },
        include:{
            business: {
                include: {
                    user: {select: {id:true,name:true}}
                }
            }
        }
    })
    return assets;
}
export const updateAsset = async(id,data) => {
    try {
        const prevasset = await prisma.asset.findFirst({where: {
            id: id
        }})
        const prepareData = {...prevasset,...data};
        if(data.propertyName) prepareData.propertyName = slugify(data.propertyName)
        let businessConn = data.businessId ? data.businessId : prepareData.businessId
        prepareData.business = { connect: { id: businessConn }}
        delete prepareData['id']
        delete prepareData['businessId']
        delete prepareData['createdAt']
        delete prepareData['updatedAt']
        delete prepareData['deleted']
        // return prepareData;
        const asset = await prisma.asset.update({
            where: {
                id: id
            },
            data:prepareData
        });
        return asset;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroyAsset = async(id) => {
    const asset = await prisma.asset.delete({
        where:{
            id: { id }
        }
    });
    return asset;
}