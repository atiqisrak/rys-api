import prisma from "../../lib/prisma";
const { slugify } = require("../../lib/helper");

export const addSubAsset = async(data) => {
    try {
        // return data;
        const subasset = await prisma.SubAsset.create({
            data: {
                asset: { connect: { id: data.assetId } },
                amenities: data.amenities,
                floor: data.floor,
                sqft: data.sqft,
                status: data.status ? data.status : true
            }
        });
        return subasset;
    } catch (error) {
        return error;
    }
}
export const getAllSubAsset = async(assetId) => {
    const subassets = await prisma.SubAsset.findMany({
        where: {
            assetId: assetId
        },
        include:{
            asset: {
                select: {id:true,propertyName:true} 
            }
        }
    })
    return subassets;
}

export const getSingleSubAsset = async(id) => {
    const subassets = await prisma.SubAsset.findMany({
        where:{
            id:id
        },
        include:{
            asset: {
                include: {
                    user: {select: {id:true,name:true}}
                }
            }
        }
    })
    return subassets;
}
export const updateSubAsset = async(id,data) => {
    try {
        const prevasset = await prisma.SubAsset.findFirst({where: {
            id: id
        }})
        const prepareData = {...prevasset,...data};
        let assetConn = data.assetId ? data.assetId : prepareData.assetId
        prepareData.asset = { connect: { id: assetConn }}
        delete prepareData['id']
        delete prepareData['assetId']
        delete prepareData['createdAt']
        delete prepareData['updatedAt']
        delete prepareData['deleted']

        const subasset = await prisma.SubAsset.update({
            where: {
                id: id
            },
            data:prepareData
        });
        return subasset;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroySubAsset = async(id) => {
    const subasset = await prisma.SubAsset.delete({
        where:{
            id: { id }
        }
    });
    return subasset;
}