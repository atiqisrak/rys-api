import prisma from "../../lib/prisma";

export const addSeatBed = async(data) => {
    try {
        const seatBed = await prisma.SeatBed.create({
            data: {
                subAssetComponent: { connect: { id: data.subAssetCompId } },
                type: data.type,
                roomNo: data.roomNo,
                breakfast: data.breakfast,
                accomodationCapacity: data.accomodationCapacity,
                extraBedPolicy: data.extraBedPolicy,
                status: data.status ? data.status : true
            }
        });
        return seatBed;
    } catch (error) {
        return error;
    }
}
export const getAllSeatBed = async(subAssetCompId,reservationCategory) => {
    const seatBeds = await prisma.SeatBed.findMany({
        where: {
            subAssetCompId:subAssetCompId,
            subAssetComponent: {
                reservationCategory: reservationCategory
            }
        },
        include: {
            subAssetComponent: {
                select: {
                    id: true,
                    listingName: true,
                    reservationCategory: true
                },
            },
        },
    })
    return seatBeds;
}

export const getSingleSeatBed = async(id) => {
    const seatBed = await prisma.SeatBed.findMany({
        where:{
            id:id
        },
        include: {
            subAssetComponent: {
                select: {
                    id: true,
                    listingName: true,
                    reservationCategory: true
                },
            },
        },
    })
    return seatBed;
}
export const updateSeatBed = async(id,data) => {
    try {
        const seatBed = await prisma.SeatBed.update({
            where: {
                id: id
            },
            data:{
                subAssetComponent: { connect: { id: data.subAssetCompId } },
                type: data.type,
                roomNo: data.roomNo,
                breakfast: data.breakfast,
                accomodationCapacity: data.accomodationCapacity,
                extraBedPolicy: data.extraBedPolicy,
                status: data.status
            },
        });
        return seatBed;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroySeatBed = async(id) => {
    const seatBed = await prisma.SeatBed.delete({
        where:{
            id: { id }
        }
    });
    return seatBed;
}