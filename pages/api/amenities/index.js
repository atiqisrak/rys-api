import { addAmenities,getAllAmenities,getSingleAmenities,updateAmenities,destroyAmenities } from "../../../controller/amenities";

export default async function handler(req,res){
    try {
        switch (req.method) {
            case "POST": {
                const amenities = await addAmenities(req.body)
                return res.status(201).json(amenities)
            }
            case "GET": {
                const {id} = req.query
                let amenities = {};
                  if(!id){
                    amenities = await getAllAmenities();
                  }else{
                    amenities = await getSingleAmenities(id);
                  }
                return res.status(200).json(amenities)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroyAmenities(id);
                return res.status(200).json({message:'Amenities Deleted Successful',asset: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const updatedamenities = await updateAmenities(id,req.body);
                return res.status(200).json({message:'Amenities Updated Successful',user: updatedamenities});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}