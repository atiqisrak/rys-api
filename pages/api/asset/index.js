import { addAsset,getAllAsset,getSingleAsset,updateAsset,destroyAsset } from "../../../controller/asset";

export default async function handler(req,res){
    try {
        switch (req.method) {
            case "POST": {
                const asset = await addAsset(req.body)
                return res.status(201).json(asset)
            }
            case "GET": {
                const {id} = req.query
                let asset = {};
                  if(!id){
                    asset = await getAllAsset();
                  }else{
                    asset = await getSingleAsset(id);
                  }
                return res.status(200).json(asset)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroyAsset(id);
                return res.status(200).json({message:'Asset Deleted Successful',asset: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const updatedAsset = await updateAsset(id,req.body);
                return res.status(200).json({message:'Asset Updated Successful',user: updatedAsset});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}