import { addSeatBed,getAllSeatBed,getSingleSeatBed,updateSeatBed,destroySeatBed } from "../../../controller/seatbed";

export default async function handler(req,res){
    try {
        switch (req.method) {
            case "POST": {
                // return res.status(201).json(req.body)
                const seatBed = await addSeatBed(req.body)
                return res.status(201).json(seatBed)
            }
            case "GET": {
                const {id} = req.query
                let seatBed = {};
                  if(!id){
                    seatBed = await getAllSeatBed();
                  }else{
                    seatBed = await getSingleSeatBed(id);
                  }
                return res.status(200).json(seatBed)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroySeatBed(id);
                return res.status(200).json({message:'Seat Bed Deleted Successful',seatBed: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const seatBed = await updateSeatBed(id,req.body);
                return res.status(200).json({message:'Seat Bed Updated Successful',seatBed: seatBed});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}