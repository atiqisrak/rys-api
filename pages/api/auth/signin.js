const jwt = require('jsonwebtoken');
import {registerUser,loginUser} from "../../../services/backendAuth"

export default async function handler(req,res){
    try {
        switch(req.method){
            case "POST": {
                const pathname = req.query;
                let authUser;
                if(pathname.for == "register-request"){
                    const { password,confirmPassword } = req.body
                    if(confirmPassword != password) return res.status(400).json({ error: 'Confirm Password are not matched',status: 400 });
                    authUser = await registerUser(req.body)
                } else {
                    authUser = await loginUser(req.body)
                }
                return res.status(201).json(authUser);
                
            }
            case "GET": {
                // const { token } = req.body
                try {
                    const secret = process.env.JWT_SECRET;
                    var authorization = req.headers.authorization.split(' ')[1];
                    // return res.status(200).json(authorization);
                    // const tok = authorization.split(' ')[1];
                    const decoded = jwt.verify(authorization, secret);
                    return res.status(200).json(decoded);
                } catch (error) {
                    return res.status(405).json({ success: false, error: error.message });
                } 
            }
            
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        // console.error('Error:', error);
        return res.status(500).json(error);
    }
}