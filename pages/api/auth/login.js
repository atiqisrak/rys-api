import {sendOtp,userUpdate,findOrCreateUser} from "../../../services/AuthService";
export default async function handler(req,res){
    try {
        switch(req.method){
            case "POST": {
                const { password, platform, isOtpVerify } = req.body
                if(platform == 'apps'){
                    const pathname = req.query;
                    const userData = await findOrCreateUser(req.body);
                    if(pathname.for == "request-otp"){
                        if (!userData) return res.status(400).json({ error: 'Incorrect login',status: 400 });
                        const setOtp = await sendOtp(userData);
                          if(setOtp){
                              return res.status(201).json({ message: 'OTP send to your number' });
                          }
                          return res.status(400).json({ error: 'Something went wrong!',status: 400 });
                    }
                    if(pathname.for == "submit-otp"){
                        const currentTime = new Date();
                        if(password != userData.otp) return res.status(400).json({ error: 'Incorrect OTP',status: 400 });
                        if(currentTime > userData.otpExpireAt) return res.status(400).json({ error: 'OTP timeout',status: 400 });
                        if(isOtpVerify == true){
                            return res.status(201).json({ moreInfo: true,user: userData });
                        } else {
                            const updateUser = await userUpdate(req.body,userData.id)
                            return res.status(200).json(updateUser);
                        }
                    }
                }
            }
            
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}