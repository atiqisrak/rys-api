const jwt = require('jsonwebtoken');

export default async function handler(req,res){
    try {
        switch(req.method){
            case "GET": {
                try {
                    const secret = process.env.JWT_SECRET;
                    var authorization = req.headers.authorization.split(' ')[1];
                    const info = jwt.verify(authorization, secret);
                    return res.status(200).json(info);
                } catch (error) {
                    return res.status(405).json({ success: false, error: error.message });
                } 
            }
            
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        // console.error('Error:', error);
        return res.status(500).json(error);
    }
}