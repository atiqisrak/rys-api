import { addTable,getAllTable,getSingleTable,updateTable,destroyTable } from "../../../controller/backendController/tableController";
import authenticate from '../../authMiddleware';

const handler= async(req,res) => {
    try {
        switch (req.method) {
            case "POST": {
                const table = await addTable(req.body)
                return res.status(201).json(table)
            }
            case "GET": {
                const {id,subAssetCompId,reservationCategory} = req.query
                // return res.status(200).json(req.query)
                let table = {};
                  if(!id){
                    table = await getAllTable(subAssetCompId,reservationCategory);
                  }else{
                    table = await getSingleTable(id);
                  }
                return res.status(200).json(table)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroyTable(id);
                return res.status(200).json({message:'Table Deleted Successful',table: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const table = await updateTable(id,req.body);
                return res.status(200).json({message:'Table Updated Successful',table: table});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}
export default authenticate(handler);