import { addAsset,getAllAsset,getSingleAsset,updateAsset,destroyAsset } from "../../../controller/backendController/assetController";
import authenticate from '../../authMiddleware';

const handler = async(req,res) => {
  try {
    switch (req.method) {
      case "POST": {
          const asset = await addAsset(req.body)
          return res.status(201).json(asset)
      }
      case "GET": {
          const {assetId} = req.query
          let asset = {};
            if(!assetId){
              asset = await getAllAsset(req.user.id);
            }else{
              asset = await getSingleAsset(assetId);
            }
          return res.status(200).json(asset)
      }
      case 'DELETE': {
          const {id} = req.query
          const destroy = await destroyAsset(id);
          return res.status(200).json({message:'Asset Deleted Successful',asset: destroy});
        }

      case 'PUT': {
          const {id} = req.query
          const updatedAsset = await updateAsset(id,req.body);
          return res.status(200).json(updatedAsset);
          return res.status(200).json({message:'Asset Updated Successful',asset: updatedAsset});
      }
      default:{
          return res.status(405).json({ message: 'Method Not Allowed' });
      }
    }
  } catch (error) {
    return res.status(500).json({ message: error });
  }
}
export default authenticate(handler)