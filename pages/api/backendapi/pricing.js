import { makePricing,getAllPricing,getSinglePricing,updatePricing,destroyPricing } from "../../../controller/backendController/pricingController";
import authenticate from '../../authMiddleware';

const handler= async(req,res) => {
    try {
        switch (req.method) {
            case "POST": {
                const pricing = await makePricing(req.body)
                return res.status(201).json(pricing)
            }
            case "GET": {
                const {id,subAssetCompId,reservationCategory} = req.query
                let seatBed = {};
                  if(!id){
                    seatBed = await getAllPricing(subAssetCompId,reservationCategory);
                  }else{
                    seatBed = await getSinglePricing(id);
                  }
                return res.status(200).json(seatBed)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroyPricing(id);
                return res.status(200).json({message:'Seat Bed Deleted Successful',seatBed: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const seatBed = await updatePricing(id,req.body);
                return res.status(200).json({message:'Seat Bed Updated Successful',seatBed: seatBed});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}
export default authenticate(handler);