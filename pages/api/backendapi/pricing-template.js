import { createPriceTemp,updatePriceTemp, destroypriceTemp, getAllPriceTemp, getPriceTemp } from "../../../controller/backendController/pricingTempController";
import authenticate from '../../authMiddleware';

const handler= async(req,res) => {
    try {
      switch(req.method){
        case 'POST': {
                  const priceTemp = await createPriceTemp(req.body);
                  return res.status(201).json(priceTemp);
              }
              case 'DELETE': {
                const {id} = req.query
                const priceTemp = await destroypriceTemp(id);
                return res.status(200).json({message:'Price Deleted Successful',priceTemp: priceTemp});
              }

              case 'PUT': {
                const {id} = req.query
                const updatedpriceTemp = await updatePriceTemp(id,req.body);
                return res.status(200).json({message:'Price Updated Successful',priceTemp: updatedpriceTemp});
              }

              case 'GET': {
                  const {id,assetId} = req.query
                  if(!id){
                    const priceTemp = await getAllPriceTemp(assetId);
                    return res.status(200).json(priceTemp);
                  }else{
                    const priceTemp = await getPriceTemp(id);
                    return res.status(200).json(priceTemp);
                  }
              }
              default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
              }
          }
      // });
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ message: 'Internal Server Error' });
    }
}

export default authenticate(handler);